## Moustache-CI docker image generator

**This project only create docker images for CI/CD, not the moustache software itself.**

## Tags

* 8.0
* 7.4
* 7.3
* 7.2
* coverage

## Publication

Images are actually published only when the branch the master branch is properly tagged.
Images are pushed on gitlab registry.
